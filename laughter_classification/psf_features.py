import numpy as np
import pandas as pd
import scipy.io.wavfile as wav
from python_speech_features import delta
from python_speech_features import logfbank
from python_speech_features import mfcc

from laughter_classification.sspnet_data_sampler import SSPNetDataSampler


def get_features_from_wav(wav_path, frame_sec):
    rate, sig = wav.read(wav_path)
    mfcc_feat = mfcc(sig, rate, winlen=frame_sec)
    fbank_feat = logfbank(sig, rate, winlen=frame_sec)
    d_mfcc_feat = delta(mfcc_feat, 2)

    big_mat = np.hstack([mfcc_feat, d_mfcc_feat, fbank_feat])
    big_df = pd.DataFrame(big_mat)
    colnames = ["MFCC{}".format(i) for i in range(mfcc_feat.shape[1])]
    colnames.extend(["FBANK{}".format(i) for i in range(fbank_feat.shape[1])])
    colnames.extend(["dMFCC{}".format(i) for i in range(d_mfcc_feat.shape[1])])
    big_df.columns = colnames
    return big_df


def sspnet_to_features(corpus_path, frame_sec, naudio=None):
    ds = SSPNetDataSampler(corpus_path)

    fullpaths = ds.get_valid_wav_paths()
    if naudio is not None:
        fullpaths = fullpaths[:naudio]
    dataframes = [get_features_from_wav(wav_path, frame_sec) for wav_path in fullpaths]
    labels = [ds.get_labels_for_file(wav_path, frame_sec) for wav_path in fullpaths]

    all_data = pd.concat(dataframes)
    all_labels = pd.concat(labels)

    return pd.concat([all_data, all_labels], axis=1)
