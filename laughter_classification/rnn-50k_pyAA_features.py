import keras
import pandas as pd
import sklearn.metrics as metrics
from keras import initializers
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.layers import Dense, Activation
from keras.layers import SimpleRNN
from keras.models import Sequential
from keras.optimizers import RMSprop
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def get_roc_auc(Y_test, Y_pred):
    if sum(Y_test[:, 1]) == 0:
        return -1
    auc = metrics.roc_auc_score(Y_test[:, 1], Y_pred[:, 1])
    return auc


class LossHistory(keras.callbacks.Callback):
    cnt = 0

    def __init__(self, model, x_val, y_val, x_train, y_train):
        self.model = model
        self.x_val = x_val
        self.y_val = y_val
        self.x_train = x_train
        self.y_train = y_train

    def on_epoch_end(self, epoch, logs=None):
        LossHistory.cnt += 1

        if LossHistory.cnt % 2 == 5:
            print("\n=====", "TRAIN", "=====\n")
            y_train_pred = self.model.predict(self.x_train)
            train_auc = get_roc_auc(self.y_train, y_train_pred)
            print("AUC   :", train_auc)

        print("\n=====", "VALIDATION", "=====\n")
        y_pred = self.model.predict(self.x_val)
        auc = get_roc_auc(self.y_val, y_pred)
        print("AUC   :", auc)


def run_RNN(X, y, model_save_path):
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)
    x_val, y_val = x_test, y_test

    batch_size = 110
    num_classes = 2
    epochs = 40
    hidden_units = 100

    learning_rate = 1e-6

    x_train = x_train.reshape(x_train.shape[0], -1, 1)
    x_val = x_val.reshape(x_val.shape[0], -1, 1)
    x_test = x_test.reshape(x_test.shape[0], -1, 1)

    print('x_train shape:', x_train.shape)
    print('y_train shape:', y_train.shape)
    print('x_val shape:', x_val.shape)
    print('y_val shape:', y_val.shape)
    print('x_test.shape:', x_test.shape)
    print('y_test.shape:', y_test.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_val = keras.utils.to_categorical(y_val, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    print('Evaluate IRNN...')
    model = Sequential()
    model.add(SimpleRNN(hidden_units,
                        kernel_initializer=initializers.RandomNormal(stddev=0.001),
                        recurrent_initializer=initializers.Identity(gain=1.0),
                        activation='relu',
                        input_shape=x_train.shape[1:]))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))
    rmsprop = RMSprop(lr=learning_rate)
    model.compile(loss='categorical_crossentropy',
                  optimizer=rmsprop,
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_val, y_val),
              callbacks=[LossHistory(model, x_val, y_val, x_train, y_train), TensorBoard(),
                         ModelCheckpoint(model_save_path, period=5)])

    scores = model.evaluate(x_test, y_test, verbose=1)
    print('IRNN test score:', scores[0])
    print('IRNN test accuracy:', scores[1])


def main():
    model_save_path = "../models/rnn_50k_pyAA_features.h5"
    nrows = 50000
    features_df = pd.read_csv("../features/feaures_pyAA_all_10ms.csv", nrows=nrows)

    nfeatures = features_df.shape[1] - 2
    X = features_df.iloc[:, :nfeatures].as_matrix()
    y = features_df.IS_LAUGHTER.as_matrix()

    scaler = StandardScaler()
    X = scaler.fit_transform(X)

    run_RNN(X, y, model_save_path)

if __name__ == '__main__':
    main()
