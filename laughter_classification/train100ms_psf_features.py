import pandas as pd

from train10ms_psf_features import test_all_sklearn


def main():
    FEATURE_PATH = "../features/features_499400rows_100ms.csv"
    nrows = 30000
    feature_df = pd.read_csv(FEATURE_PATH, nrows=nrows)
    nfeatures = feature_df.shape[1] - 2
    X = feature_df.loc[:, :'F{}'.format(nfeatures - 1)].as_matrix()
    y = feature_df.IS_LAUGHTER.as_matrix()
    test_all_sklearn(X, y)

if __name__ == '__main__':
    main()
