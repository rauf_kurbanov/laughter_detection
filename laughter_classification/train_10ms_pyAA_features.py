import pandas as pd

from train10ms_psf_features import test_all_sklearn


def main():
    FEATURE_PATH = "../features/feaures_pyAA_all_10ms.csv"
    nrows = 30000
    feature_df = pd.read_csv(FEATURE_PATH, nrows=nrows)
    nfeatures = feature_df.shape[1] - 2
    only_features = feature_df.iloc[:, :nfeatures]
    X = only_features.as_matrix()
    y = feature_df.IS_LAUGHTER.as_matrix()
    test_all_sklearn(X, y)

if __name__ == '__main__':
    main()
