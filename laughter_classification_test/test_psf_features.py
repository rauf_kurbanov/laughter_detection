from unittest import TestCase

from laughter_classification.psf_features import sspnet_to_features, get_features_from_wav


class TestFeatures(TestCase):
    def setUp(self):
        self.corpus_root = "/home/kurbanov/Data/vocalizationcorpus"

    def test_sspnet_to_features(self):
        df = sspnet_to_features(self.corpus_root, frame_sec=0.01, naudio=5)
        self.assertEqual(df.shape, (5500, 54))

    def test_get_features_from_wav(self):
        wav_path = "/home/kurbanov/Data/vocalizationcorpus/data/S0123.wav"
        df = get_features_from_wav(wav_path, frame_sec=0.01)
        self.assertEqual(df.shape, (1100, 52))
