from unittest import TestCase

from laughter_classification.sspnet_data_sampler import SSPNetDataSampler


class TestSSPNetDataSampler(TestCase):
    def setUp(self):
        CORPUS_ROOT = "/home/kurbanov/Data/vocalizationcorpus"
        self.ds = SSPNetDataSampler(CORPUS_ROOT)

    def test_create_df(self):
        df = self.ds.create_sampled_df(frame_sec=0.01, naudio=10)
        self.assertEqual(df.shape, (11000, 162))

    def test_df_from_file(self):
        wav_path = "/home/kurbanov/Data/vocalizationcorpus/data/S0123.wav"
        df = self.ds.df_from_file(wav_path, frame_sec=0.01)
        self.assertEqual(df.shape, (1100, 162))
