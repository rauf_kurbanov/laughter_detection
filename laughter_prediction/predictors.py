import numpy as np
import pickle

from sklearn.externals import joblib
from keras.models import load_model
from sklearn.preprocessing import StandardScaler


class Predictor:
    """
    Wrapper class used for loading serialized model and
    using it in classification task.
    Defines unified interface for all inherited predictors.
    """

    def predict(self, X):
        """
        Predict target values of X given a model

        :param X: numpy.ndarray, dtype=float, shape=[n_samples, n_features]
        :return: numpy.array predicted classes
        """
        raise NotImplementedError("Should have implemented this")

    def predict_proba(self, X):
        """
        Predict probabilities of target class

        :param X: numpy.ndarray, dtype=float, shape=[n_samples, n_features]
        :return: numpy.array target class probabilities
        """
        raise NotImplementedError("Should have implemented this")


class XgboostPredictor(Predictor):
    """Parametrized wrapper for xgboost-based predictors"""

    def __init__(self, model_path, threshold, scaler=None):
        self.threshold = threshold
        self.clf = joblib.load(model_path)
        self.scaler = scaler

    def _simple_smooth(self, data, n=50):
        dlen = len(data)

        def low_pass(data, i, n):
            if i < n // 2:
                return data[:i]
            if i >= dlen - n // 2 - 1:
                return data[i:]
            return data[i - n // 2: i + n - n // 2]

        sliced = np.array([low_pass(data, i, n) for i in range(dlen)])
        sumz = np.array([np.sum(x) for x in sliced])
        return sumz / n

    def predict(self, X):
        y_pred = self.clf.predict_proba(X)
        ypreds_bin = np.where(y_pred[:, 1] >= self.threshold, np.ones(len(y_pred)), np.zeros(len(y_pred)))
        return ypreds_bin

    def predict_proba(self, X):
        X_scaled = self.scaler.fit_transform(X) if self.scaler is not None else X
        not_smooth = self.clf.predict_proba(X_scaled)[:, 1]
        return self._simple_smooth(not_smooth)


class MiniXgboostPredictor(XgboostPredictor):
    """
    Predictor trained on 0.5kk training examples, using PyAAExtractor
    for input features
    """

    def __init__(self, threshold=0.17902173):
        XgboostPredictor.__init__(self, model_path="models/XGBClassifier_0.5kk_pyAA10.pkl",
                                  threshold=threshold)


class StrictLargeXgboostPredictor(XgboostPredictor):
    """
    Predictor trained on 3kk training examples, using PyAAExtractor
    for input features
    """
    def __init__(self, threshold=0.045985743):
        XgboostPredictor.__init__(self, model_path="models/XGBClassifier_3kk_pyAA10.pkl",
                                  threshold=threshold, scaler=StandardScaler())


class AutoMlPredictor(Predictor):
    """Complex cascade predictor trained by auto-sklearn"""

    def __init__(self, threshold=0.19774633):
        model_path = "models/AutoSklearnClassifier_0.5kk_pyAA10.pickle"
        self.threshold = threshold
        self.clf = pickle.load(open(model_path, "rb"))

    def predict(self, X):
        y_pred = self.clf.predict_proba(X)
        ypreds_bin = np.where(y_pred[:, 1] >= self.threshold, np.ones(len(y_pred)), np.zeros(len(y_pred)))
        return ypreds_bin

    def predict_proba(self, X):
        return self.clf.predict_proba(X)[:, 1]


class TinyRnnPredictor(Predictor):

    def __init__(self):
        model_path = "models/rnn_50k_pyAA_features.h5"
        self.clf = load_model(model_path)
        self.threshold = 0.03329942
        self.scaler = StandardScaler()

    @staticmethod
    def _simple_smooth(data, n=50):
        dlen = len(data)

        def low_pass(data, i, n):
            if i < n // 2:
                return data[:i]
            if i >= dlen - n // 2 - 1:
                return data[i:]
            return data[i - n // 2: i + n - n // 2]

        sliced = np.array([low_pass(data, i, n) for i in range(dlen)])
        sumz = np.array([np.sum(x) for x in sliced])
        return sumz / n

    def predict(self, X):
        y_pred = self.predict_proba(X)
        ypreds_bin = np.where(y_pred >= self.threshold, np.ones(len(y_pred)), np.zeros(len(y_pred)))
        return ypreds_bin

    def predict_proba(self, X):
        X_scaled = self.scaler.fit_transform(X)
        not_smooth = self.clf.predict_proba(X.reshape(X_scaled.shape[0], -1, 1))[:, 1]
        return self._simple_smooth(not_smooth)
